﻿using System;
using System.Collections.Generic;

namespace Lists
{
    class Program
    {
        class User
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public int Age { get; set; }
        }
        static void Main(string[] args)
        {
            List<User> uzytkownicy = new List<User>();
            uzytkownicy.Add(new User 
            {
                FirstName = "Jacek", 
                LastName = "Nowicki", 
                Age = 28 
            });
            uzytkownicy.Add(new User
            {
                FirstName = "Michał",
                LastName = "Nowak",
                Age = 39
            });
            uzytkownicy.Add(new User
            {
                FirstName = "Anna",
                LastName = "Kościelna",
                Age = 14
            });
            uzytkownicy.Add(new User
            {
                FirstName = "Zbigniew",
                LastName = "Json",
                Age = 40
            });
            uzytkownicy.Add(new User
            {
                FirstName = "Grzegorz",
                LastName = "Pawłocki",
                Age = 90
            });

            var znaleziony = uzytkownicy.Find(x => x.Age == 30 && x.FirstName == "Zbigniew");
            var znalezionaLista = uzytkownicy.FindAll(user => user.Age < 40);

            //int[] tablica = new int[] { 1, 2, 3, 4, 5 };

            //List<int> lista = new List<int>();

            //lista.Add(5);
            //bool czyUsunieto = lista.Remove(5);
            //int znalezionyElement = lista.Find(x => x == 5);
            //bool istnieje = lista.Exists(x => x == 5);

        }
    }
}
