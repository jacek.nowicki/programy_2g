﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FirstWpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int value = 0;
        private string lastPressedButton = string.Empty;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button button &&
                button.Tag is string tag)
            {
                switch (tag)
                {
                    case "1":
                        ShowNumber("1");
                        break;

                    case "2":
                        ShowNumber("2");
                        break;

                    case "3":
                        ShowNumber("3");
                        break;

                    case "4":
                        ShowNumber("4");
                        break;

                    case "5":
                        ShowNumber("5");
                        break;

                    case "6":
                        ShowNumber("6");
                        break;

                    case "7":
                        ShowNumber("7");
                        break;

                    case "8":
                        ShowNumber("8");
                        break;

                    case "9":
                        ShowNumber("9");
                        break;

                    case "0":
                        ShowNumber("0");
                        break;

                    case "+":
                        AddNumber();
                        break;

                    case "-":
                        SubstractNumber();
                        break;

                    case "=":
                        ShowResult();
                        break;

                    default:
                        break;
                }
            }
        }

        private void ShowNumber(string number)
        {
            if (!(string.IsNullOrEmpty(Result.Text) &&
                number.Equals("0")))
            {
                Result.Text += number;
            }
        }

        private void AddNumber()
        {
            value = Convert.ToInt32(Result.Text);
            Result.Text = string.Empty;
            lastPressedButton = "+";
        }

        private void SubstractNumber()
        {
            value = Convert.ToInt32(Result.Text);
            Result.Text = string.Empty;
            lastPressedButton = "-";
        }

        private void ShowResult()
        {
            switch (lastPressedButton)
            {
                case "+":
                    value += Convert.ToInt32(Result.Text);
                    Result.Text = value.ToString();
                    break;

                case "-":
                    value -= Convert.ToInt32(Result.Text);
                    Result.Text = value.ToString();
                    break;
            }
        }
    }
}
