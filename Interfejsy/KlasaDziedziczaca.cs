﻿namespace Interfejsy
{
    public class KlasaDziedziczaca : ITrzeciInterfejs
    {
        public int Width { get; set; }

        public int ObliczIloczyn(int a, int b)
        {
            return a * b;
        }

        public int ObliczSume(int a, int b)
        {
            return a + b;
        }
    }
}
