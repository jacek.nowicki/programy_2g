﻿namespace Interfejsy
{
    public interface IDrugiInterfejs
    {
        int ObliczIloczyn(int a, int b);
        int Width { get; set; }
    }
}