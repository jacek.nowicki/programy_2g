﻿using System;

namespace Interfejsy
{
    class Program
    {
        static void Main(string[] args)
        {
            KlasaDziedziczaca obiekt = new KlasaDziedziczaca();
            int wynik = obiekt.ObliczSume(2, 5);
            int wynikMnozenia = obiekt.ObliczIloczyn(2, 5);
            Console.WriteLine(wynik);

            IPierwszyInterfejs pierwszy = obiekt;
            int suma = pierwszy.ObliczSume(2, 5);
            
            IDrugiInterfejs drugi = obiekt;
            int iloczyn = drugi.ObliczIloczyn(2, 5);

            ITrzeciInterfejs trzeci = obiekt;
            int wynik3Iloczyn = trzeci.ObliczIloczyn(2, 6);
            int wynik3Suma = trzeci.ObliczSume(2, 6);

        }
    }
}
