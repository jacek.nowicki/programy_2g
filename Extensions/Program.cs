﻿using Personel;
using System;

namespace Extensions
{
    class Program
    {
        static void Main(string[] args)
        {
            User nowyUser = new User("Jan", "Kowalski", 50, "666");
            string x = nowyUser.ToString();
            Console.WriteLine(nowyUser);
            Console.WriteLine($"{nowyUser}");
        }
    }
}
