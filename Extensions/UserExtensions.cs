﻿using Personel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extensions
{
    public static class UserExtensions
    {
        //public static void WypiszDane(this User user)
        //{
        //    Console.WriteLine($"{user.FirstName} {user.LastName} {user.Age}");
        //}

        public static void DodajLata(this User user, int lata)
        {
            user.Age += lata;
        }

        public static int ZwrocRokUrodzenia(this User user)
        {
            return DateTime.Now.Year - user.Age;
        }
    }
}
