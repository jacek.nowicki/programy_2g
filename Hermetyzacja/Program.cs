﻿using System;

namespace Hermetyzacja
{
    class DbConnection
    {
        public string Address { get; private set; }
        public int Port { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }

        public DbConnection(string address, int port, string username, string password)
        {
            this.Address = address;
            this.Port = port;
            this.Username = username;
            this.Password = password;
        }

        public void ChangeCred(string address, int port, string username, string password)
        {
            // Tutaj zakończymy połączenie
            // i upewnimy się, że jest ok.

            this.Address = address;
            this.Port = port;
            this.Username = username;
            this.Password = password;

            // Tutaj przywracamy połączenie
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            DbConnection dbConn = new DbConnection("192.168.0.1", 1881, "tester", "123"); // Tworzymy nowy obiekt typy DbConnection

            var x = dbConn.Address; // Tutaj mamy możliwość odczytu danych
            dbConn.Address = "asdasdas"; // Tutaj nie możemy zmienić wartości zmiennej
        }
    }
}
