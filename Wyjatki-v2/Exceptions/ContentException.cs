﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Wyjatki_v2.Exceptions
{
    /// <summary>
    /// The exception that is thrown when ...
    /// </summary>
    [Serializable]
    public class ContentException : Exception
    {
        public string FileName { get; }


        /// <summary>
        /// Initializes a new instance of the <see cref="ContentException"/> class.
        /// </summary>
        public ContentException() : base()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentException"/> class with serialized data.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="StreamingContext"/> that contains contextual information about the source or destination.</param>
        protected ContentException(SerializationInfo info, StreamingContext context) : base(info, context)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentException"/> class with the specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public ContentException(string message) : base(message)
        { }

        public ContentException(string message, string fileName) : base (message)
        {
            this.FileName = fileName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentException"/> class with a specified error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="innerException">The inner exception reference.</param>
        public ContentException(string message, Exception innerException) : base(message, innerException)
        { }

        public ContentException(string message, string fileName, Exception innerException)
            : base (message, innerException)
        {
            this.FileName = fileName;
        }
    }
}
