﻿using System;
using System.IO;
using Wyjatki_v2.Exceptions;

namespace Wyjatki_v2
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = $"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}\\file.txt";
            //string path = $"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}\\file2.txt";
            //string path = "C:\\notExistingCatalog\\file2.txt";
            //string path = null;

            try
            {
                string fileContent = File.ReadAllText($"{path}");
                Console.WriteLine($"Zawartość odczytanego pliku to: {fileContent}");
                throw new ContentException("Nastąpił błąd odczytu pliku.", path);
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine("Nie znaleziono pliku!");
                Console.WriteLine($"{ex.Message}");
            }
            catch (DirectoryNotFoundException ex)
            {
                Console.WriteLine("Nie znaleziono katalogu!");
                Console.WriteLine($"{ex.Message}");
            }
            catch (ContentException ex)
            {
                Console.WriteLine($"Wystąpił błąd odczutu pliku: {ex.FileName}");
                Console.WriteLine($"{ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
            }
            finally
            {
                Console.WriteLine($"Kończę działanie...");
            }
        }
    }
}
