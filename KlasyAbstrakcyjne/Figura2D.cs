﻿namespace KlasyAbstrakcyjne
{ 
    public abstract class Figura2D
    {
        public abstract double Pole();

        public abstract double Obwod();

        public abstract double IloscKatow();
    }
}
