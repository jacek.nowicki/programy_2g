﻿using KlasyAbstrakcyjne.Figury;
using System;

namespace KlasyAbstrakcyjne
{
    class Program
    {
        static void Main(string[] args)
        {
            Kolo kolo = new Kolo(2.5);
            Kwadrat kwadrat = new Kwadrat(12.5);

            double poleKola = kolo.Pole();
            double poleKw = kwadrat.Pole();

            Figura2D kolo2 = new Kolo(5);
            Figura2D kwadrat2 = new Kwadrat(10);

            Figura2D[] tablicaFigur = new Figura2D[]
            {
                kolo,
                kwadrat,
                kolo2,
                kwadrat2
            };

            foreach(var figura in tablicaFigur)
            {
                if (figura.GetType() == typeof(Kolo))
                {
                    Console.WriteLine($"Pole Koła to: {figura.Pole()}");
                }

                if (figura.GetType() == typeof(Kwadrat))
                {
                    Console.WriteLine($"Pole kwadratu to: {figura.Pole()}");
                }
            }
        
        }
    }
}
