﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlasyAbstrakcyjne.Figury
{
    public class TrojkatRownoramienny : Figura2D
    {
        public double Podstawa { get; set; }
        public double Wysokosc { get; set; }

        public TrojkatRownoramienny(double podstawa, double wysokosc)
        {
            Podstawa = podstawa;
            Wysokosc = wysokosc;
        }

        public override double Pole()
        {
            return Podstawa * Wysokosc / 2;
        }

        public override double Obwod()
        {
            double bok = Math.Sqrt(Math.Pow((Podstawa / 2), 2) + Math.Pow(Wysokosc, 2));
            return Podstawa + (bok * 2);
        }

        public override double IloscKatow()
        {
            return 3;
        }
    }
}
