﻿using System;

namespace KlasyAbstrakcyjne.Figury
{
    public class Kolo : Figura2D
    {
        public double Promien { get; set; }

        public Kolo(double promien)
        {
            Promien = promien;
        }

        public override double Pole()
        {
            return Math.PI * Math.Pow(Promien, 2);
        }

        public override double Obwod()
        {
            return 2 * Math.PI * Promien;
        }

        public override double IloscKatow()
        {
            return 0;
        }
    }
}
