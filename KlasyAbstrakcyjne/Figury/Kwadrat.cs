﻿namespace KlasyAbstrakcyjne.Figury
{
    public class Kwadrat : Figura2D
    {
        public double DlugoscBoku { get; set; }

        public Kwadrat(double dlugoscBoku)
        {
            DlugoscBoku = dlugoscBoku;
        }

        public override double Pole()
        {
            return DlugoscBoku * DlugoscBoku;
        }

        public override double Obwod()
        {
            return DlugoscBoku * 4;
        }

        public override double IloscKatow()
        {
            return 4;
        }
    }
}
