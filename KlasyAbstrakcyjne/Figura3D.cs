﻿namespace KlasyAbstrakcyjne
{
    public class Figura3D
    {
        public virtual double PolePowierzchni()
        {
            return default;
        }

        public virtual double DlugoscKrawedzi()
        {
            return default;
        }
    }
}
