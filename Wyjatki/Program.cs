﻿using System;
using System.Runtime.Serialization;

namespace Wyjatki
{
    /// <summary>
    /// The exception that is thrown when ...
    /// </summary>
    [Serializable]
    public class MyException : Exception
    {
        public int MojePole { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MyException"/> class.
        /// </summary>
        public MyException() : base()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="MyException"/> class with serialized data.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="StreamingContext"/> that contains contextual information about the source or destination.</param>
        protected MyException(SerializationInfo info, StreamingContext context) : base(info, context)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="MyException"/> class with the specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public MyException(string message) : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="MyException"/> class with a specified error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="innerException">The inner exception reference.</param>
        public MyException(string message, Exception innerException) : base(message, innerException)
        { }
    }

    class Program
    {
        public static void MetodaWyjatkowa(string parametr)
        {
            throw new ArgumentException("Komunikat wyjątku...", nameof(parametr));
        }

        static void Main(string[] args)
        {
            try
            {
                MetodaWyjatkowa("aaaa");
            }
            catch (ArgumentException ex)
            {
                throw new MyException("Moja wiadomosc", ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Console.WriteLine("Koniec programu...");
            }
        }
    }
}
