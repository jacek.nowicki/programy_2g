﻿using System;
using System.Collections.Generic;

namespace Personel
{
    class Program
    {
        private static List<User> users = new List<User>();

        static void Main(string[] args)
        {
            int[] uss = new int[] { 234, 765, 78, 87 };
            int[] arr = new int[10];

            int[,] arr2d = new int[10, 10];

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    arr2d[i, j] = 10;
                }
            }

            bool working = true;
            while (working)
            {
                int wybranaOpcja = PrintMainMenu();

                switch (wybranaOpcja)
                {
                    case 1:
                        AddNewUser();
                        break;

                    case 2:
                        ShowAllUsers();
                        break;

                    case 3:
                        DeleteUser();
                        break;

                    case 4:
                        working = false;
                        break;
                }
            }
        }

        static int PrintMainMenu()
        {
            Console.Clear();
            Console.WriteLine("====== USER MANAGER ======");
            Console.WriteLine("[1] - Dodaj nowego użytkownika");
            Console.WriteLine("[2] - Wyświetl listę użytkowników");
            Console.WriteLine("[3] - Usuń użytkownika");
            Console.WriteLine("[X] - Wyjscie z programu");

            while (true)
            {
                var wybor = Console.ReadKey();

                switch (wybor.Key)
                {
                    case ConsoleKey.D1:
                        return 1;

                    case ConsoleKey.D2:
                        return 2;

                    case ConsoleKey.D3:
                        return 3;

                    case ConsoleKey.X:
                        return 4;
                }
            }
        }

        static void AddNewUser()
        {
            string firstName, lastName, age, telephone;

            Console.Clear();
            Console.WriteLine("===== NOWY UŻYTKOWNIK =====");
            Console.Write("Imie:");
            firstName = Console.ReadLine();

            Console.Write("Nazwisko:");
            lastName = Console.ReadLine();

            Console.Write("Wiek:");
            age = Console.ReadLine();

            Console.Write("Telefon:");
            telephone = Console.ReadLine();

            User newUser = new User(firstName, lastName, Convert.ToInt32(age), telephone);

            users.Add(newUser);
        }

        static void ShowAllUsers()
        {
            Console.Clear();
            Console.WriteLine("===== WYSWIETLANIE UŻYTKOWNIKÓW =====");
            Console.WriteLine("Imie | Nazwisko | Wiek | Telefon");

            foreach (var user in users)
            {
                Console.WriteLine($"{user.FirstName} | {user.LastName} | {user.Age} | {user.Telephone}");
            }
            Console.ReadKey();
        }

        static void DeleteUser()
        {

        }
    }
}
