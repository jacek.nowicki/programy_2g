﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Personel
{
    public class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Telephone { get; set; }

        public User(string firstName, string lastName, int age, string telephone)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            Telephone = telephone;
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName} {Age} {Telephone}";
        }
    }
}
