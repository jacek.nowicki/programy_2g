﻿using Operator_Is.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operator_Is.Figury3D
{
    public class Walec : IFigura3D
    {
        public double Promien { get; set; }
        public double Wysokosc { get; set; }

        public double DlugoscKrawedzi()
        {
            return 4 * Math.PI * Promien;
        }

        public double Objetosc()
        {
            return Wysokosc * Math.PI * Math.Pow(Promien, 2);
        }
    }
}
