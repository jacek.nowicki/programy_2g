﻿using Operator_Is.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operator_Is.Figury3D
{
    public class Szescian : IFigura3D
    {
        public double Bok { get; set; }

        public double DlugoscKrawedzi()
        {
            return Bok * 6;
        }

        public double Objetosc()
        {
            return Math.Pow(Bok, 3);
        }
    }
}
