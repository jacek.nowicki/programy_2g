﻿using Operator_Is.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operator_Is.Figury2D
{
    public class Prostokat : IFigura2D
    {
        public double Szerokosc { get; set; }
        public double Dlugosc { get; set; }

        public double Obwod()
        {
            return 2 * Szerokosc + 2 * Dlugosc;
        }

        public double Pole()
        {
            return Szerokosc * Dlugosc;
        }
    }
}
