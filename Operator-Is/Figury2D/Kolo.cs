﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Operator_Is.Interfaces;

namespace Operator_Is.Figury2D
{
    public class Kolo : IFigura2D
    {
        public double Promien { get; set; }

        public double Obwod()
        {
            // 2 * PI * r
            return 2 * Math.PI * Promien;
        }

        public double Pole()
        {
            // PI * r^2
            // Math.Pow(podstawa potegi, wykladnik potegi)
            return Math.PI * Math.Pow(Promien, 2);
        }
    }
}
