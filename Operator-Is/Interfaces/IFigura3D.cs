﻿namespace Operator_Is.Interfaces
{
    public interface IFigura3D
    {
        public double Objetosc();
        public double DlugoscKrawedzi();
    }
}
