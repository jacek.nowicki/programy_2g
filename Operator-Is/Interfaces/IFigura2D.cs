﻿namespace Operator_Is.Interfaces
{
    public interface IFigura2D
    {
        public double Pole();
        public double Obwod();
    }
}
