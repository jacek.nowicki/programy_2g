﻿using Operator_Is.Figury2D;
using Operator_Is.Figury3D;
using Operator_Is.Interfaces;
using System;

namespace Operator_Is
{
    class Program
    {
        static void Wyswietl2D(IFigura2D figura)
        {
            Console.WriteLine($"Pole figury: {figura.Pole()}");
            Console.WriteLine($"Obwód figury: {figura.Obwod()}");
        }

        static void Wyswietl3D(IFigura3D figura)
        {
            Console.WriteLine($"Objetość figury: {figura.Objetosc()}");
            Console.WriteLine($"Dlugosc krawędzi figury: {figura.DlugoscKrawedzi()}");
        }

        static void Main(string[] args)
        {
            // Tworzenie nowych obiektów
            Kolo kolo = new Kolo { Promien = 2.7 };
            Prostokat prostokat = new Prostokat { Dlugosc = 10, Szerokosc = 12 };
            Szescian szescian = new Szescian { Bok = 3.5 };
            Walec walec = new Walec { Promien = 5.4, Wysokosc = 9.9 };

            // Wyświetlanie figur dwuwymiarowych
            Wyswietl2D(kolo);
            Wyswietl2D(prostokat);

            // Wyświetlanie figur trójwymiarowych
            Wyswietl3D(szescian);
            Wyswietl3D(walec);
        }
    }
}
