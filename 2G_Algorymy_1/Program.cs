﻿using System;

namespace _2G_Algorymy_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Podaj pierwszą liczbę: ");
            var x = Convert.ToInt32(Console.ReadLine());

            Console.Write("Podaj drugą liczbę: ");
            var y = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine($"Podane argumenty: x = {x}, y = {y}");
            Console.WriteLine($"NWD to: {NWD(x, y)}");
            Console.WriteLine($"DZI to: {DZI(x, y)}");
            Console.WriteLine($"REK to: {REK(x, y)}");

            Console.WriteLine($"POT to: {POT(x, y)}");
            Console.WriteLine($"POT_REK to: {POT_REK(x, y)}");
        }

        static int NWD(int a, int b)
        {
            if (b < 0)
                return 1;

            while (a != b)
            {
                int c, d;

                if (a > b)
                {
                    c = b;
                    d = a - b;
                }
                else
                {
                    c = a;
                    d = b - a;
                }

                a = c;
                b = d;
            }

            return a;
        }

        static int DZI(int a, int b)
        {
            while (b != 0)
            {
                int c = b;
                int d = a % b;
                a = c;
                b = d;
            }

            return a;
        }

        static int REK(int a, int b)
        {
            if (b == 0)
                return a;
            else
                return REK(b, a % b);
        }

        static int POT(int x, int n)
        {
            int y = x;
            for (int i = 0; i < n - 1; i++)
            {
                y = y * x;
            }

            return y;
        }

        static int POT_REK(int x, int n)
        {
            if (n > 0)
                return x * POT_REK(x, n - 1);
            else if (n == 0)
                return 1;
            else
                throw new Exception("N jest mniejsze od zera!");
        }
    }
}
