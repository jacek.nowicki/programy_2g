﻿using System;

namespace Tablice
{
    class Program
    {
        static void Main(string[] args)
        {
            int size = Convert.ToInt32(Console.ReadLine());

            // Tworzenie tablicy
            int[,] tabliczka = new int[size, size];

            // Wypełnianie wartościami
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    tabliczka[i, j] = (i + 1) * (j + 1);
                }
            }

            // Wyświetlanie zawartości tablicy
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write($" {tabliczka[i, j]} ");
                }

                Console.WriteLine();
            }
        }
    }
}
