﻿namespace PodstawyDziedziczenia
{
    public class Pojazd
    {
        public int IloscKol;
        public int PojemnoscSilnika;
        public string Kolor;
        public int IloscMiejsc;

        private int zmiennaPrywatna;
        protected int zmiennaChroniona;

        public Pojazd()
        {
            IloscKol = 4;
            PojemnoscSilnika = 1000;
            Kolor = "Czarny";
            IloscMiejsc = 4;
        }

        public Pojazd(int iloscKol, int pojemnoscSilnika, 
            string kolor, int iloscMiejsc)
        {
            IloscKol = iloscKol;
            PojemnoscSilnika = pojemnoscSilnika;
            Kolor = kolor;
            IloscMiejsc = iloscMiejsc;
        }
    }
}
