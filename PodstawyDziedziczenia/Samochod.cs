﻿namespace PodstawyDziedziczenia
{
    public class Samochod : Pojazd
    {
        public int IloscDrzwi;

        public Samochod()
        {
            IloscDrzwi = 3;
        }

        public Samochod(int iloscDrzwi, int iloscKol,
            int pojemnoscSilnika, string kolor, int iloscMiejsc)
            : base(iloscKol, pojemnoscSilnika, kolor, iloscMiejsc)
        {
            IloscDrzwi = iloscDrzwi;
        }
    }
}
