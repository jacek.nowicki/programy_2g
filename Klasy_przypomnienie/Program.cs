﻿using System;

namespace Klasy_przypomnienie
{
    class Ksiazka
    {
        public int IloscStron { get; set; }
        public string Tytul { get; set; }
        public string Autor { get; set; }
        public string Wydawnictwo { get; set; }
        public int RokWydania { get; set; }

        public Ksiazka()
        {

        }

        public Ksiazka(int iloscStron, string tytul,
            string autor, string wydawnictwo, int rokWydania)
        {
            IloscStron = iloscStron;
            Tytul = tytul;
            Autor = autor;
            Wydawnictwo = wydawnictwo;
            RokWydania = rokWydania;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Ksiazka nowaKsiazka = new Ksiazka(20, "aaa", "bb", "cc", 2000);
            Ksiazka nowaKsiazka2 = new Ksiazka();

            Ksiazka nowaKsiazka3 = new Ksiazka()
            {
                Autor = "aaa",
                RokWydania = 2000,
                IloscStron = 20,
                Wydawnictwo = "vvv",
                Tytul = "ccc"
            };


            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Yellow");

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Blue");
        }
    }
}
