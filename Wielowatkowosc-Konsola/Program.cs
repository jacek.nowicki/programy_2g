﻿using System;
using System.Threading.Tasks;

namespace Wielowatkowosc_Konsola
{
    public class Kuchnia
    {
        public async Task PrzygotowanieJedzenia()
        {
            await GotowanieWody();
            await GotowanieJajek();
            string stanKotleta = await SmazenieKotleta(4000);
            Console.WriteLine(stanKotleta);
        }

        public async Task<string> SmazenieKotleta(int czas)
        {
            await Task.Delay(czas);

            if (czas < 3000)
                return "Kotlet jest surowy!";
            else if (czas > 6000)
                return "Kotlet jest spalony!";
            else
                return "Kotlet jest dobry!";
        }

        public async Task GotowanieWody()
        {
            Console.WriteLine("Zaczynam gotować wodę na herbatę!");
            await Task.Delay(5000);
            Console.WriteLine("Woda na herbatę ugotowana!");
        }

        public async Task GotowanieJajek()
        {
            Console.WriteLine("Zaczynam gotować jajka");
            await Task.Delay(10000);
            Console.WriteLine("Jajka ugotowane!");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Kuchnia kuchnia = new Kuchnia();

            Task t1 = kuchnia.PrzygotowanieJedzenia();
            t1.Wait();

            Task t2 = kuchnia.GotowanieWody();
            Task t3 = kuchnia.GotowanieJajek();
            Task.WaitAll(t2, t3);

            Task.WaitAll(kuchnia.GotowanieWody(),
                         kuchnia.GotowanieJajek());
        }
    }
}
