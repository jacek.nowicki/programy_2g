﻿using System;
using System.Collections.Generic;

namespace TypyGeneryczne
{
    class Program
    {
        static void Main(string[] args)
        {
            MyList<int> inty = new MyList<int>();
            MyList<string> stringi = new MyList<string>();

            inty.Add(3);
            inty.Add(10);
            inty.Add(50);

            int iloscElementow = inty.Count;
            int wartosc = inty[2];

            for(int i = 0; i < inty.Count; i++)
            {
                Console.WriteLine($"Wartosc {i}-go elementu: {inty[i]}");
            }


            // Porównania metodami generycznymi...
            Porownanie<int>(1, 2);
        }

        // 1. Metody wykorzystujące 'sztywno' określone parametry
        public static bool Porownanie(int parametr1, int parametr2)
        {
            throw new NotImplementedException();
        }
        public static bool Porownanie(double parameter1, double parameter2)
        {
            throw new NotImplementedException();
        }

        // 2. Metoda wykorzystująca typ object
        public static bool Porownanie(object parameter1, object parameter2)
        {
            if (parameter1.GetType() == parameter2.GetType())
            {
                throw new NotImplementedException();
            }
            throw new NotImplementedException();
        }

        public static bool Porownanie<T>(T parameter1, T parameter2)
            where T : IComparable<T>
        {
            return parameter1.CompareTo(parameter2) == 0;
        }
    }
}
