﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypyGeneryczne
{
    public class MyList<T>
    {
        private T[] items;

        public T this[int index] { get => this.items[index]; }

        // Właściwość wykorzystująca operator lambda
        public int Count { get => items.Length; }

        // Zwykła właściwość
        //public int Count 
        //{ 
        //    get 
        //    { 
        //        return items.Length; 
        //    } 
        //}

        // Metoda zwracająca wartość
        //public int Count()
        //{
        //    return items.Length;
        //}

        public void Add(T element)
        {
            if (items == null)
                items = new T[] { element };
            else
                items = items.Append(element).ToArray();
        }
    }
}
