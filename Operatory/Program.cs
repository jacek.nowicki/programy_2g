﻿using System;

namespace Operatory
{
    public class Prostokat
    {
        public int Wysokosc { get; private set; }
        public int Szerokosc { get; private set; }

        public Prostokat(int wysokosc, int szerokosc)
        {
            this.Wysokosc = wysokosc;
            this.Szerokosc = szerokosc;
        }

        // Przeciążenia
        public static Prostokat operator+(Prostokat p1, Prostokat p2)
        {
            int sumaSzerokosci = p1.Szerokosc + p2.Szerokosc;
            int sumaWysokosci = p1.Wysokosc + p2.Wysokosc;

            Prostokat suma = new Prostokat(sumaWysokosci, sumaSzerokosci);

            return suma;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Prostokat p1 = new Prostokat(10, 20);
            Prostokat p2 = new Prostokat(5, 30);

            Prostokat p3 = p1 + p2;

        }
    }
}
