﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Szkola
{
    public class Uczen : Osoba
    {
        public string NumerLegitymacji { get; }

        public override string Imie { get; }

        public override string Nazwisko { get; }

        public Uczen(string imie, string nazwisko, string numerLegitymacji)
        {
            Imie = imie;
            Nazwisko = nazwisko;
            NumerLegitymacji = numerLegitymacji;
        }

        public override void WypiszDane()
        {
            Console.WriteLine($"Imie: {Imie}, Nazwisko: {Nazwisko}, Nr Legitymacji: {NumerLegitymacji}");
        }
    }
}
