﻿using System;

namespace Szkola
{
    class Program
    {
        static void Main(string[] args)
        {
            Uczen uczen = new Uczen("Jan", "Kowalski", "87654");
            uczen.WypiszDane();

            Osoba uczenOsoba = (Osoba)uczen;
            uczenOsoba.WypiszDane();
        }
    }
}
