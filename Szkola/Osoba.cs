﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Szkola
{
    public abstract class Osoba
    {
        public abstract string Imie { get; }
        public abstract string Nazwisko { get; }

        public virtual void WypiszDane()
        {
            Console.WriteLine($"Imię: {Imie}, Nazwisko: {Nazwisko}");
        }
    }
}
