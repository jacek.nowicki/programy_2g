﻿using System;

namespace Klasy
{
    class Program
    {
        static void Main(string[] args)
        {
            Samochod fiat = new Samochod("Biały", 1000, 85, "Hatchback", "Welur");
            Samochod ford = new Samochod(
                pojemnosc: 2000,
                moc: 200,
                kolor: "Czerwony",
                typNadwozia: "Sedan",
                typTapicerki: "Skóra");

            int pojemnoscForda = ford.GetPojemnosc();

            ford.Zawarcz("", 12);
            var x = ford.Dodaj(1, 1);

            int i = 1;
            int j = ZwiekszOJeden(i);

            Console.WriteLine($"{i}, {j}");

            int k = 1;
            int l = ZwiekszOJedenRef(ref k);

            Console.WriteLine($"{k}, {l}");

        }

        static int ZwiekszOJeden(int x)
        {
            x++;
            return x;
        }

        static int ZwiekszOJedenRef(ref int x)
        {
            x++;
            return x;
        }
    }

    class Samochod
    {
        private string kolor;
        private int pojemnosc;
        private int moc;
        private string typNadwozia;
        private string typTapicerki;

        public string Kolor { get; private set; }

        public int Pojemnosc
        {
            get
            {
                return pojemnosc;
            }

            set
            {
                pojemnosc = value;
            }
        }

        public Samochod()
        {
            kolor = "Biały";
            pojemnosc = 1000;
            moc = 100;
            typNadwozia = "Hatchback";
            typTapicerki = "Welur";
        }

        public Samochod(string kolor, int pojemnosc, int moc, string typNadwozia, string typTapicerki)
        {
            this.kolor = kolor;
            this.pojemnosc = pojemnosc;
            this.moc = moc;
            this.typNadwozia = typNadwozia;
            this.typTapicerki = typTapicerki;
        }

        public Samochod(int pojemnosc)
        {
            this.pojemnosc = pojemnosc;
        }

        public Samochod(Samochod obj)
        {
            Kolor = obj.Kolor;
            Pojemnosc = obj.Pojemnosc;
        }



        public int GetPojemnosc()
        {
            return pojemnosc;
        }
        public void SetPojemnosc(int pojemnosc)
        {
            this.pojemnosc = pojemnosc;
        }

        public void Zawarcz()
        {
            Console.WriteLine($"Zawarczało {pojemnosc} cm3!!!");
        }

        public void Zawarcz(int czas)
        {
            Console.WriteLine($"Zawarczało {pojemnosc} cm3 przez {czas} sekund!");
        }

        public void Zawarcz(string x)
        {

        }

        public void Zawarcz(string pojemnosc, int czas)
        {

        }

        public void Zawarcz(int czas, string x)
        {

        }


        public int Dodaj(int a, int b)
        {
            return a + b;
        }

        public double Dodaj(double a, double b)
        {
            return a + b;
        }
    }
}
