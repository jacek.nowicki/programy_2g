﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wyjatki_GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenMi_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new();

                if (openFileDialog.ShowDialog() == true)
                {
                    if (openFileDialog.FileName.Contains(".txt"))
                    {
                        string content = File.ReadAllText(openFileDialog.FileName);
                        ContentTb.Text = content;
                    }
                    else
                    {
                        throw new Exception("Plik nie ma rozszerzenia .txt");
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd odczytu", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
